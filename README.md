# PagesJaunes

### Installation instructions

1. [Download](https://gitlab.com/renxiao/pagesjaunes/-/archive/master/pagesjaunes-master.zip) the project
2. Unzip it
3. Open with Xcode 12 `pagesjaunes-master/PagesJaunes/PagesJaunes.xcodeproj`
4. Choose a simulator or a connected device, then CMD + R to run the project
