//
//  Favorite.swift
//  PagesJaunes
//
//  Created by Renxiao Mo on 23/02/2021.
//

import Foundation

struct Favorite: Identifiable {
    let id = UUID()
    let listingId: String
    let proId: String
    let name: String
    let address: String
    let phoneNumber: String
    let thumbnailUrl: String?
    
    init(professional: SearchListing) {
        self.listingId = professional.listingId
        self.proId = professional.inscriptions.first?.proId ?? ""
        self.name = professional.merchantName

        var formattedAddress = ""
        if let inscription = professional.inscriptions.first {
            formattedAddress = "\(inscription.street) \(inscription.zipCode) \(inscription.city)"
        }
        self.address = formattedAddress
        
        self.phoneNumber = professional.inscriptions.first?.contactInfo.first?.contactValue ?? ""
        self.thumbnailUrl = professional.thumbnailUrl
    }
}

extension Favorite: Equatable {
    static func ==(lhs: Favorite, rhs: Favorite) -> Bool {
        return lhs.listingId == rhs.listingId
            && lhs.proId == rhs.proId
            && lhs.name == rhs.name
            && lhs.address == rhs.address
            && lhs.phoneNumber == rhs.phoneNumber
            && lhs.thumbnailUrl == rhs.thumbnailUrl
    }
}
