//
//  FavoritesManager.swift
//  PagesJaunes
//
//  Created by Renxiao Mo on 23/02/2021.
//

import Foundation

protocol FavoritesProvider {
    var favorites: [SearchListing] { get }
    
    func isFavorite(_ favorite: SearchListing) -> Bool
    func markAsFavorite(_ favorite: SearchListing)
    func removeFavorite(_ favorite: SearchListing)
}

class FavoritesManager {
 
    static let shared = FavoritesManager()
    
    private var _favorites = [SearchListing]()
    
    private init() {}
}

extension FavoritesManager: FavoritesProvider {
    
    var favorites: [SearchListing] {
        return _favorites
    }
    
    func isFavorite(_ favorite: SearchListing) -> Bool {
        return _favorites.filter({ $0 == favorite }).count != 0
    }
    
    func markAsFavorite(_ favorite: SearchListing) {
        _favorites.append(favorite)
    }
    
    func removeFavorite(_ favorite: SearchListing) {
        guard let foundFavoriteIndex = _favorites.firstIndex(where: { $0 == favorite }) else { return }
        _favorites.remove(at: foundFavoriteIndex)
    }
}
