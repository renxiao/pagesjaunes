//
//  NetworkClient.swift
//  PagesJaunes
//
//  Created by Renxiao Mo on 23/02/2021.
//

import Foundation

enum CustomError: Error {
    case invalidUrl
}

enum RequestResult<T> {
    case success(T)
    case fail(Error)
}

protocol SearchProvider {
    func search(what: String, location: String, completion: @escaping ((RequestResult<SearchResponse>) -> Void))
}

class NetworkClient: SearchProvider {
    
    private func authenticate(_ url: String = Constant.API.authURL, completion: @escaping ((RequestResult<String>) -> Void)) {
        guard let url = URL(string: url) else { return }
        
        var components = URLComponents(url: url, resolvingAgainstBaseURL: false)

        components?.queryItems = [
            URLQueryItem(name: "grant_type", value: Constant.API.grantType),
            URLQueryItem(name: "client_id", value: Constant.API.clientID),
            URLQueryItem(name: "client_secret", value: Constant.API.clientSecret)
        ]

        guard let query = components?.url?.query else { return }

        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        request.httpBody = Data(query.utf8)
        
        let task = URLSession.shared.dataTask(with: request) { (data, response, error) in
            if let error = error {
                print("error: \(error)")
                completion(.fail(error))
            } else {
                if let response = response as? HTTPURLResponse {
                    print("statusCode: \(response.statusCode)")
                }
                if let data = data, let dataString = String(data: data, encoding: .utf8) {
                    print("data: \(dataString)")
                    
                    let decoder = JSONDecoder()

                    do {
                        let tokenResponse = try decoder.decode(TokenResponse.self, from: data)
                        
                        completion(.success(tokenResponse.accessToken))

                    } catch let error {
                        print("🔴 Error when decoding NetworkClientData : \(error)")
                    }
                }
            }
        }
        task.resume()
    }
    
    func search(what: String, location: String = "paris", completion: @escaping ((RequestResult<SearchResponse>) -> Void)) {
        authenticate { tokenResponse in
            switch tokenResponse {
            case .success(let token):
                guard let url = URL(string: "https://api.pagesjaunes.fr/v1/pros/search?what=\(what)&where=\(location)") else {
                    completion(.fail(CustomError.invalidUrl))
                    return
                }
                
                var request = URLRequest(url: url)
                request.addValue("Bearer \(token)", forHTTPHeaderField: "Authorization")
                request.addValue("application/json", forHTTPHeaderField: "Content-Type")
                request.addValue("application/json", forHTTPHeaderField: "Accept")
                request.httpMethod = "GET"

                URLSession.shared.dataTask(with: request) { (data, response , error) in
                    guard let error = error else {
                        guard let data = data else { return }
                        
                        let decoder = JSONDecoder()
                        do {
                            let searchResponse = try decoder.decode(SearchResponse.self, from: data)
                            
                            print(searchResponse)
                            completion(.success(searchResponse))
                        } catch let error {
                            print("🔴 Error when decoding SearchResponse : \(error)")
                        }
                        return
                    }
                    completion(.fail(error))
                }.resume()
            case .fail(let error):
                completion(.fail(error))
            }
        }
    }
}
