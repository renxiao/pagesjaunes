//
//  SearchResponse.swift
//  PagesJaunes
//
//  Created by Renxiao Mo on 23/02/2021.
//

import Foundation

struct SearchResponse: Decodable {
    let searchResults: SearchResults
    
    enum CodingKeys: String, CodingKey {
        case searchResults = "search_results"
    }
}

struct SearchResults: Decodable {
    let listings: [SearchListing]
}

struct SearchListing: Decodable, Identifiable {
    let id = UUID()
    let listingId: String
    let merchantName: String
    let thumbnailUrl: String?
    let inscriptions: [Inscription]
    
    enum CodingKeys: String, CodingKey {
        case listingId = "listing_id"
        case merchantName = "merchant_name"
        case thumbnailUrl = "thumbnail_url"
        case inscriptions = "inscriptions"
    }
}

extension SearchListing: Equatable {
    static func ==(lhs: SearchListing, rhs: SearchListing) -> Bool {
        return lhs.listingId == rhs.listingId
            && lhs.merchantName == rhs.merchantName
            && lhs.thumbnailUrl == rhs.thumbnailUrl
            && lhs.inscriptions == rhs.inscriptions
    }
}

struct Inscription: Decodable {
    let city: String
    let street: String
    let zipCode: String
    let contactInfo: [ContactInfo]
    let proId: String
    
    enum CodingKeys: String, CodingKey {
        case city = "address_city"
        case street = "address_street"
        case zipCode = "address_zipcode"
        case contactInfo = "contact_info"
        case proId = "pro_id"
    }
}

extension Inscription: Equatable {
    static func ==(lhs: Inscription, rhs: Inscription) -> Bool {
        return lhs.city == rhs.city
            && lhs.street == rhs.street
            && lhs.zipCode == rhs.zipCode
            && lhs.contactInfo == rhs.contactInfo
            && lhs.proId == rhs.proId
    }
}

struct ContactInfo: Decodable {
    let contactValue: String
    
    enum CodingKeys: String, CodingKey {
        case contactValue = "contact_value"
    }
}

extension ContactInfo: Equatable {}
