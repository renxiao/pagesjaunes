//
//  TokenResponse.swift
//  PagesJaunes
//
//  Created by Renxiao Mo on 23/02/2021.
//

import Foundation

struct TokenResponse: Decodable {
    let accessToken: String
    
    enum CodingKeys: String, CodingKey {
        case accessToken = "access_token"
    }
}
