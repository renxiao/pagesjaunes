//
//  ContentView.swift
//  PagesJaunes
//
//  Created by Renxiao Mo on 23/02/2021.
//

import SwiftUI

struct ContentView: View {

    @State private var selection = 0
    
    var body: some View {
        TabBarWrapper(selection: $selection) {
            [SearchView().tabItem(title: "", image: UIImage(named: "magnifying_glass")),
             FavoritesView().tabItem(title: "", image: UIImage(named: "heart"))
            ]
        }
        .edgesIgnoringSafeArea(.all)
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
