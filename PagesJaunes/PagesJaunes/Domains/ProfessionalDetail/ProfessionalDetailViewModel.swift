//
//  ProfessionalDetailViewModel.swift
//  PagesJaunes
//
//  Created by Renxiao Mo on 23/02/2021.
//

import SwiftUI

class ProfessionalDetailViewModel: ObservableObject {
    
    private let professional: SearchListing
    private let favoritesProvider: FavoritesProvider
    
    let name: String
    let address: String
    let phoneNumber: String
    
    @Published var isFavorite = false
    
    init(professional: SearchListing, favoritesProvider: FavoritesProvider = FavoritesManager.shared) {
        self.professional = professional
        self.favoritesProvider = favoritesProvider
        self.name = professional.merchantName

        var formattedAddress = ""
        if let inscription = professional.inscriptions.first {
            formattedAddress = "\(inscription.street) \(inscription.zipCode) \(inscription.city)"
        }
        self.address = formattedAddress
        self.phoneNumber = professional.inscriptions.first?.contactInfo.first?.contactValue ?? ""
        
        self.isFavorite = favoritesProvider.isFavorite(professional)
    }
    
    func makePhoneCall() {
        guard let number = URL(string: "tel://" + phoneNumber.replace(target: " ", withString: "")) else { return }
        UIApplication.shared.open(number)
    }
    
    func markAsFavorite() {
        favoritesProvider.markAsFavorite(professional)

        isFavorite = favoritesProvider.isFavorite(professional)
    }
    
    func removeFavorite() {
        favoritesProvider.removeFavorite(professional)
        
        isFavorite = favoritesProvider.isFavorite(professional)
    }
    
    func reloadFavorites() {
        isFavorite = favoritesProvider.isFavorite(professional)
    }
}
