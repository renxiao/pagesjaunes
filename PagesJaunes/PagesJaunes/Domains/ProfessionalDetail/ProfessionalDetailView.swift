//
//  ProfessionalDetailView.swift
//  PagesJaunes
//
//  Created by Renxiao Mo on 23/02/2021.
//

import SwiftUI

struct ProfessionalDetailView: View {
    
    @SwiftUI.Environment(\.presentationMode) var presentation
    
    @ObservedObject var viewModel: ProfessionalDetailViewModel
    
    var body: some View {
        ScrollView {
            VStack(spacing: Constant.spacing) {
                Text(viewModel.name)
                    .font(.title)
                    .multilineTextAlignment(.center)
                Text(viewModel.address)
                    .multilineTextAlignment(.center)
                
                Button(action: {
                    viewModel.makePhoneCall()
                }, label: {
                    Text(viewModel.phoneNumber)
                })
            }
        }
        .frame(maxWidth: /*@START_MENU_TOKEN@*/.infinity/*@END_MENU_TOKEN@*/, maxHeight: /*@START_MENU_TOKEN@*/.infinity/*@END_MENU_TOKEN@*/)
        .padding()
        .navigationBarBackButtonHidden(true)
        .navigationBarItems(leading: Button(action: { presentation.wrappedValue.dismiss() }) {
            BackButton()
        }, trailing: Button(action: {
            if viewModel.isFavorite {
                viewModel.removeFavorite()
            } else {
                viewModel.markAsFavorite()
            }
        }, label: {
            Image(viewModel.isFavorite ? "red_heart_fill" : "red_heart")
                .renderingMode(.original)
        }))
        .onAppear(perform: {
            viewModel.reloadFavorites()
        })
    }
}

struct BackButton: View {
    var body: some View {
        HStack {
            Image(systemName: "chevron.left")
                .foregroundColor(.black)
                .imageScale(.large)
            Spacer()
        }
        .frame(width: 44, height: 44)
    }
}
