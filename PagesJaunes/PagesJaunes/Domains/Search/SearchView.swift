//
//  SearchView.swift
//  PagesJaunes
//
//  Created by Renxiao Mo on 23/02/2021.
//

import SwiftUI

struct SearchObject {
    let what: String
    let location: String
}

struct SearchView: View {
    
    @State private var what = ""
    @State private var location = ""
    
    var whatString: String {
        return what
    }
    
    var locationString: String {
        return location
    }
    
    var isSearchDisabled: Bool {
        return what.isEmpty || location.isEmpty
    }
    
    var body: some View {
        VStack(spacing: Constant.spacing * 2) {
            VStack {
                TextField("Que recherchez-vous ?", text: $what)
                    .multilineTextAlignment(.center)
                    .modifier(ClearButton(text: $what))
                TextField("Où ?", text: $location)
                    .multilineTextAlignment(.center)
                    .modifier(ClearButton(text: $location))
            }
            
            NavigationLink(destination: ProfessionalsView(viewModel: ProfessionalsViewModel(what: what, location: location))) {
                Text("Rechercher")
                    .fontWeight(.medium)
                    .modifier(SearchButton(disabled: isSearchDisabled))
            }
            .disabled(isSearchDisabled)
        }
        .navigationBarTitle("PagesJaunes")
    }
}
