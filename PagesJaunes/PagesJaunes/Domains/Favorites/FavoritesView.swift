//
//  FavoritesView.swift
//  PagesJaunes
//
//  Created by Renxiao Mo on 23/02/2021.
//

import SwiftUI


struct FavoritesView: View {
    
    @ObservedObject var viewModel = FavoritesViewModel()
    
    var body: some View {
        
        VStack {
            if viewModel.favorites.isEmpty {
                EmptyView(title: "Bienvenue dans votre espace Favoris !",
                          description: "Vous n'avez pas encore enregistré vos pros préférés.")
            } else {
                ScrollView {
                    VStack(spacing: Constant.spacing) {
                        ForEach(viewModel.favorites) { favorite in
                            NavigationLink(destination:
                                            ProfessionalDetailView(viewModel: ProfessionalDetailViewModel(professional: favorite))) {
                                ProfessionalRow(viewModel: rowViewModel(fromFavorite: favorite))
                            }
                        }
                    }
                    .padding()
                }
                .background(Color.black.opacity(0.1))
                .frame(maxWidth: .infinity, maxHeight: .infinity)
            }
        }
        .navigationBarTitle("Favoris")
        .onAppear(perform: {
            viewModel.reloadFavorites()
        })
    }
    
    private func rowViewModel(fromFavorite favorite: SearchListing) -> ProfessionalRowViewModel {
        return ProfessionalRowViewModel(professional: favorite)
    }
}
