//
//  FavoritesViewModel.swift
//  PagesJaunes
//
//  Created by Renxiao Mo on 23/02/2021.
//

import Foundation

class FavoritesViewModel: ObservableObject {

    private let favoritesProvider: FavoritesProvider
    
    @Published var favorites: [SearchListing]
    
    init(favoritesProvider: FavoritesProvider = FavoritesManager.shared) {
        self.favoritesProvider = favoritesProvider
        
        self.favorites = favoritesProvider.favorites
    }
    
    func reloadFavorites() {
        favorites = favoritesProvider.favorites
    }
}
