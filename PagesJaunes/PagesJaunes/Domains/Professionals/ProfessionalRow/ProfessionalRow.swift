//
//  ProfessionalRow.swift
//  PagesJaunes
//
//  Created by Renxiao Mo on 23/02/2021.
//

import SwiftUI

struct ProfessionalRow: View {
    
    let viewModel: ProfessionalRowViewModel
    
    @State var image: UIImage = UIImage(named: Constant.placeholder) ?? UIImage()
    
    var body: some View {
        VStack(spacing: Constant.spacing) {
            VStack(spacing: 2) {
                HStack {
                    Text(viewModel.name)
                        .foregroundColor(.black)
                        .font(.headline)
                    Spacer()
                }
                HStack {
                    Text(viewModel.address)
                        .foregroundColor(.gray)
                        .font(.caption)
                    Spacer()
                }
            }
            
            HStack {
                Image(uiImage: image)
                    .renderingMode(.original)
                    .resizable()
                    .aspectRatio(contentMode: .fill)
                    .frame(width: Constant.thumbnailWidth, height: Constant.thumbnailWidth)
                    .clipped()
                    .cornerRadius(Constant.cornerRadius)
                    .onReceive(viewModel.imageLoader.didChange) { data in
                        self.image = UIImage(data: data) ?? UIImage()
                    }
                Spacer()
            }
        }
        .padding()
        .background(Color.white)
        .cornerRadius(Constant.cornerRadius)
    }
}

private extension Constant {
    static let thumbnailWidth: CGFloat = 70
    static let placeholder = "placeholder"
}
