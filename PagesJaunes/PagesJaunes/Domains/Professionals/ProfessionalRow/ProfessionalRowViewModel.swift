//
//  ProfessionalRowViewModel.swift
//  PagesJaunes
//
//  Created by Renxiao Mo on 23/02/2021.
//

import SwiftUI

struct ProfessionalRowViewModel: Identifiable {
    let id = UUID()
    let name: String
    let address: String
    let thumbnailUrl: String?
    
    @ObservedObject var imageLoader: ImageLoader
    
    init(name: String, address: String, thumbnailUrl: String?) {
        self.name = name
        self.address = address
        self.thumbnailUrl = thumbnailUrl
        
        self.imageLoader = ImageLoader(urlString: thumbnailUrl ?? "")
    }
    
    init(professional: SearchListing) {
        self.name = professional.merchantName
        
        var formattedAddress: String = ""
        
        if let address = professional.inscriptions.first {
            formattedAddress = "\(address.street) \(address.zipCode) \(address.city)"
        }
        self.address = formattedAddress
        self.thumbnailUrl = professional.thumbnailUrl
        self.imageLoader = ImageLoader(urlString: thumbnailUrl ?? "")
    }
}
