//
//  ProfessionalsView.swift
//  PagesJaunes
//
//  Created by Renxiao Mo on 23/02/2021.
//

import SwiftUI

struct ProfessionalsView: View {
    
    @SwiftUI.Environment(\.presentationMode) var presentation
    
    @ObservedObject var viewModel: ProfessionalsViewModel
    
    var body: some View {
        ScrollView {
            VStack(spacing: Constant.spacing) {
                ForEach(viewModel.professionals) { professional in
                    
                    NavigationLink(destination: ProfessionalDetailView(viewModel: ProfessionalDetailViewModel(professional: professional))) {
                        ProfessionalRow(viewModel: ProfessionalRowViewModel(professional: professional))
                    }
                }
            }
            .frame(maxWidth: /*@START_MENU_TOKEN@*/.infinity/*@END_MENU_TOKEN@*/, maxHeight: /*@START_MENU_TOKEN@*/.infinity/*@END_MENU_TOKEN@*/)
            .padding()
        }
        .background(Color.black.opacity(0.1))
        .navigationBarTitle(viewModel.navigationTitle)
        .navigationBarBackButtonHidden(true)
        .navigationBarItems(leading: Button(action: { presentation.wrappedValue.dismiss() }) {
            BackButton()
        })
        .onAppear {
            viewModel.fetchProfessionals()
        }
    }
}
