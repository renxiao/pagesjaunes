//
//  ProfessionalsViewModel.swift
//  PagesJaunes
//
//  Created by Renxiao Mo on 23/02/2021.
//

import Foundation

class ProfessionalsViewModel: ObservableObject {
    
    private let what: String
    private let location: String
    private let searchProvider: SearchProvider
    
    @Published var professionals: [SearchListing] = []
    
    var navigationTitle: String {
        return what
    }
    
    init(what: String, location: String, searchProvider: SearchProvider = NetworkClient()) {
        self.what = what
        self.location = location
        self.searchProvider = searchProvider
    }
    
    func fetchProfessionals() {
        guard professionals.isEmpty else { return }
        searchProvider.search(what: what, location: location) { requestResult in
            switch requestResult {
            case .success(let searchResponse):
                DispatchQueue.main.async {
                    self.professionals = searchResponse.searchResults.listings
                }
                
                print(self.professionals)
            case .fail(let error):
                print(error.localizedDescription)
            }
        }
    }
    
    func childViewModels() -> [ProfessionalRowViewModel] {
        return professionals.map { professional in
            
            var formattedAddress: String = ""
            
            if let address = professional.inscriptions.first {
                formattedAddress = "\(address.street) \(address.zipCode) \(address.city)"
            }
            
            return ProfessionalRowViewModel(name: professional.merchantName,
                                            address: formattedAddress,
                                            thumbnailUrl: professional.thumbnailUrl)
        }
    }
}
