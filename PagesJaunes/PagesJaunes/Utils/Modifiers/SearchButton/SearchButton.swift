//
//  SearchButton.swift
//  PagesJaunes
//
//  Created by Renxiao Mo on 23/02/2021.
//

import SwiftUI

struct SearchButton: ViewModifier {
    
    var disabled: Bool
    
    func body(content: Content) -> some View {
        content
            .foregroundColor(disabled ? .gray : .black)
            .padding(.horizontal, Constant.spacing)
            .padding(.vertical, Constant.spacing / 2)
            .background(disabled ? Color.gray.opacity(0.2) : Color.appYellow)
            .cornerRadius(Constant.cornerRadius)
            .padding(.vertical, 1)
    }
}
