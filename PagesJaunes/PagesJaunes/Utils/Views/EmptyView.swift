//
//  EmptyView.swift
//  PagesJaunes
//
//  Created by Renxiao Mo on 23/02/2021.
//

import SwiftUI

struct EmptyView: View {
    let title: String
    let description: String
    
    var body: some View {
        VStack(spacing: Constant.spacing) {
            Text(title)
                .font(.headline)
            Text(description)
                .font(.caption)
        }
    }
}
