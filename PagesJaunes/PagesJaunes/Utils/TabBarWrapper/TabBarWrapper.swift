//
//  TabBarWrapper.swift
//  PagesJaunes
//
//  Created by Renxiao Mo on 23/02/2021.
//

import SwiftUI

struct TabBarWrapper: UIViewControllerRepresentable {
    @Binding var selection: Int
        
    var tabVC: [UINavigationController]
    
    init(selection: Binding<Int>, tabs tabVC: () -> [TabItem]) {
        self._selection = selection
        
        self.tabVC = tabVC().map { item in
            let hostVC = UIHostingController(rootView: item.contentView)
            hostVC.tabBarItem = item.item
            return UINavigationController(rootViewController: hostVC)
        }
    }
    
    func makeUIViewController(context: Context) -> UITabBarController {
        let tabBarVC = UITabBarController()
        setUpTabBar(tabBarVC, context: context)
        return tabBarVC
    }
    
    func setUpTabBar(_ tabVC: UITabBarController, context: Context) {
        tabVC.viewControllers = self.tabVC
        tabVC.delegate = context.coordinator
    }
    
    func updateUIViewController(_ uiViewController: UITabBarController, context: Context) {
    }

    typealias UIViewControllerType = UITabBarController
    
    func makeCoordinator() -> Coordinator {
        return Coordinator(selection: $selection)
    }
    
    class Coordinator: NSObject, UITabBarControllerDelegate {
        @Binding var selection: Int
        
        init(selection: Binding<Int>) {
            self._selection = selection
        }
        
        func tabBarController(_ tabBarController: UITabBarController, didSelect viewController: UIViewController) {
            self.selection = tabBarController.selectedIndex
        }
    }
    
    struct TabItem {
        var item: UITabBarItem
        var contentView: AnyView
        
        init<T:View>(item: UITabBarItem, view contentView: T) {
            self.item = item
            self.contentView = AnyView(contentView)
        }
        
        init<T:View>(title: String? = "", image: UIImage? = nil, selectedImage: UIImage? = nil, view contentView: () -> T) {
            let item  = UITabBarItem(title: title, image: image, selectedImage: selectedImage)
            item.selectedImage = selectedImage
            self.init(item: item, view: contentView())
        }
    }
}

extension View {
    func tabItem(title: String?, image: UIImage? = nil, selectedImage: UIImage? = nil) -> TabBarWrapper.TabItem {
        return TabBarWrapper.TabItem(title: title, image: image, selectedImage: image) { self }
    }
}
