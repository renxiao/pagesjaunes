//
//  Constant.swift
//  PagesJaunes
//
//  Created by Renxiao Mo on 23/02/2021.
//

import SwiftUI

enum Constant {
    enum API {
        static let authURL = "https://api.pagesjaunes.fr/oauth/client_credential/accesstoken"
        static let grantType = "client_credentials"
        static let clientID = "TseKuupeNYXQGcqkUGGiB7TR7ZrQxAx0"
        static let clientSecret = "weqVGx4qGCEZzhHO"
    }
    
    static let spacing: CGFloat = 16
    static let cornerRadius: CGFloat = 4
}
