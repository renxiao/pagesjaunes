//
//  Color+Styles.swift
//  PagesJaunes
//
//  Created by Renxiao Mo on 23/02/2021.
//

import SwiftUI

extension Color {
    static let appYellow = Color("yellow")
}
