//
//  SceneDelegate+Appearance.swift
//  PagesJaunes
//
//  Created by Renxiao Mo on 23/02/2021.
//

import SwiftUI

extension SceneDelegate {
    
    func configureAppearance() {
        let tabBarAppearance = UITabBar.appearance()
        tabBarAppearance.backgroundImage = UIImage()
        tabBarAppearance.backgroundColor = .white
        tabBarAppearance.tintColor = .black

        
        let navigationBarAppearance = UINavigationBarAppearance()
        navigationBarAppearance.configureWithOpaqueBackground()

        UINavigationBar.appearance().standardAppearance = navigationBarAppearance
        UINavigationBar.appearance().tintColor = .black
    }
}
